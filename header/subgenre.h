#ifndef SUBGENRE_H
#define SUBGENRE_H

#include <iostream>
#include <string>

/** \class SubGenre
 */

class Subgenre
{

  // Attributs

 private:
  int id;
  std::string subType;

 public:


  // Méthodes
  // Constructeurs

/** \brief Default constructor
   *
   * With null #id and #name
   */

  Subgenre();
   /** \brief Detailed constructor
   *
   * Construct a #SubGenre object based on the detailed parameterization
   *
   * \param[in] _id The #SubGenre identification number
   *
   * \param[in] _name The #SubGenre name string
   */


  Subgenre(int, std::string);


/** \brief destructeur
*/
  ~Subgenre();


/** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */

  int getId();

  /** \brief Set the object's #id entification number
   *
   * \param[in] _id The number to use as identifier
   */

  std::string getSubType();

  
  void setSubType(std::string);


};


#endif // SUBGENRE_H

