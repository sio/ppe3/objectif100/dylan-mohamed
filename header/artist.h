#ifndef ARTIST_H
#define ARTIST_H

#include <iostream>
#include <string>


/** \class Artist
 *
*/
class Artist
{
  //Propriétés
 private:
  unsigned int id;///< Object identifier
  std::string name;///< Artist name string



 public:


  //méthodes


  //Constructeurs
  //Constructeurs/destructeur

  /** \brief Default constructor
   *
   * With null #id and #name
   */

  Artist();
  /** \brief Detailed constructor
   *
   * Construct a #Artist object based on the detailed parameterization
   *
   * \param[in] _id The identification number
   *
   * \param[in] _name The #Artist's name string
   */
  Artist(unsigned int, std::string);

  /** \brief destructeur
  */
  ~Artist();

/** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */

  int getId();
   /** \brief Get the object's #name
   *
   * \return A copy of the #name \c std::string
   */

  std::string getName();

  /** \brief Set the object's #name
   *
   * \param[in] _name The \c std::string to use as #name
   */

  void setName(std::string);

};
#endif //ARTIST_H
