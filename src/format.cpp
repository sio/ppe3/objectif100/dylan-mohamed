#include <iostream>
#include "../header/format.h"


using namespace std;

//Implémentation du constructeur par défaut 

Format::Format():

id(0),
libelle(""),

{
    cout<<"Création dun format";
}

Format::Format(int _id,
                std::string _libelle):
id(_id),
libelle(_libelle)

{
    cout<<"Création d'un format avec paramètres";
}

Format::~Format(){};

int Format::getId(){
   return id;
}

std::string Format::getLibelle()
{
    return libelle;
}
