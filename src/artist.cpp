#include <iostream>
#include "../header/artist.h"


using namespace std;

//Implémentation du constructeur par défaut

Artist::Artist():

id(0),
name("")

{
    cout << "Création d'un artiste";
}

Artist::Artist(unsigned int _id,
            std::string _name):
id(_id),
name(_name)

{
  cout << "Création d'un artiste avec paramètres";
}

Artist::~Artist(){};

int Artist::getId()
{
   return id;
}

std::string Artist::getName()
{
    return name;
}
