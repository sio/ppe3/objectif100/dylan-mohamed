#include <iostream>
#include "../header/polyphony.h"


using namespace std;

// Implémentation du constructeur par défaut

Polyphony::Polyphony():
  id(0),
  number(0)
    {
      cout <<"Création d'une polyphonie";
    }

Polyphony::Polyphony(int _id,int _number):
  id(_id),
  number(_number)

{
  cout << "Création d'une polyphonie avec paramètres";
}

Polyphony::~Polyphony(){};

int Polyphony::getId(){
  return id;
}

int Polyphony::getNumber()
{
  return number;
}

void Polyphony::setNumber(int _number){

  _number=number;
}
